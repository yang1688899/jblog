package com.newflypig.jblog.util;

import javax.servlet.http.HttpSession;

import com.newflypig.jblog.exception.LoginException;

/**
 *	blog工具类 
 *	@author newflypig
 *	time：2015年12月4日
 *
 */
public class BlogUtils {
	/**
	 * 判断是否已经登录
	 * @param session
	 * @throws RuntimeException
	 */
	public static void checkPermission(HttpSession session) throws RuntimeException{
		if(session.getAttribute(BlogConfig.SYSTEM)==null)
			throw new LoginException("You do not have Permission to perform this Operation,Please Login first.");
	}
	
	/**
	 * HTML转义
	 * @param content
	 * @return
	 */
	public static String html(String content) {
		if (content == null)
			return "";
		
		content=content
			.replaceAll( "'", "&apos;")
			.replaceAll("\"", "&quot;") // "
			.replaceAll("\t", "&nbsp;&nbsp;")// 替换跳格
			.replaceAll(" ",  "&nbsp;")// 替换空格
			.replaceAll("<",  "&lt;")
			.replaceAll(">",  "&gt;");

		return content;
	}
}
