package com.newflypig.jblog.service;

import com.newflypig.jblog.model.Comment;

/**
 *	定义关于Comment类的特别服务方法
 *	time：2015年11月28日
 *
 */
public interface CommentService extends BaseService<Comment>{
}
