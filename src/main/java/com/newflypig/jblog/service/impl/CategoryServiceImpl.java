package com.newflypig.jblog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.newflypig.jblog.dao.BaseDAO;
import com.newflypig.jblog.dao.CategoryDAO;
import com.newflypig.jblog.model.Category;
import com.newflypig.jblog.service.CategoryService;

@Service("categoryService")
public class CategoryServiceImpl extends BaseServiceImpl<Category> implements CategoryService{
	
	@Autowired
	private CategoryDAO categoryDao;

	//通过这种方式向每个service植入不同的dao
	@Override
	protected BaseDAO<Category> getDao() {
		return this.categoryDao;
	}
}
