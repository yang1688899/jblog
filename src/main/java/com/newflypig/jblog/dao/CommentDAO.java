package com.newflypig.jblog.dao;

import com.newflypig.jblog.model.Comment;

/**
 *  定义关于Comment类的数据库特别操作
 *	time：2015年11月28日
 *
 */
public interface CommentDAO extends BaseDAO<Comment>{
}
