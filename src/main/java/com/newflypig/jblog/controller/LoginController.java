package com.newflypig.jblog.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.code.kaptcha.Constants;
import com.newflypig.jblog.util.BlogConfig;
import com.newflypig.jblog.service.SystemService;
import com.newflypig.jblog.exception.LoginException;
import com.newflypig.jblog.model.System;

/**
 *  第一次登陆时不需要输入验证码
 *	用户输入错误后需要输入验证码
 *	第一次GET请求login页面时，检查session中是否有post_num
 *    如果没有：session创建post_num参数，初始化为1，同时随机生成验证码captcha放入session中，页面显示时判断post_num，如果为1，则不需要用户输入验证码，如果>1，则需要用户输入验证码，生成PNG图片时到session中去检索captcha并模糊出来。
 *    如果有post_num，则post_num+1，如果post_num大于一个自定义阀值时，设置某种拒绝登陆的惩罚时间，或者将事件登记到数据库（日志文件）中。
 *	@author newflypig
 *	time：2015年12月2日
 */

@Controller
public class LoginController {
	@Resource(name="systemService")
	private SystemService systemService;
	
	
	
	/**
	 *	处理浏览器登陆GET页面
	 */
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login(HttpSession session){
		//如果已经登录，则返回blog列表，否则判断post_num
		//	post_num为空说明第一次登录，初始化post_num
		//	如果session中的punish_time大于当前时间，则拒绝登录请求，否则remove punish_time,放行登录
		if(session.getAttribute(BlogConfig.SYSTEM)==null){
			if(session.getAttribute(BlogConfig.POST_NUM)==null){
				session.setAttribute(BlogConfig.POST_NUM, new Integer(1));
				return "login";
			}else{
				Date punish_time=(Date)session.getAttribute(BlogConfig.PUNISH_TIME);
				if(punish_time==null)
					return "login";
				else if(punish_time.compareTo(new Date())>0){
					throw new LoginException("Login Denied!You Can Login after "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(punish_time));
				}else{
					session.removeAttribute(BlogConfig.POST_NUM);
					session.removeAttribute(BlogConfig.PUNISH_TIME);
					return "login";
				}
			}
		}			
		else
			return "redirect:article/articles";
	}
	
	/**
	 *	处理浏览器登陆POST请求 
	 */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(String userName,String userPwd,@RequestParam(name="captcha",required=false) String captcha,HttpSession session){
		Integer post_num=(Integer)session.getAttribute(BlogConfig.POST_NUM);
		
		//如果处理post请求时，session中没有post_num，则表示用户正在外部跨域post，需要拦截
		if(post_num==null)
			throw new LoginException("Illegal invasion,you must stop!",session);
		
		//如果处理post请求时，post_num=1，则无需用户输入验证码，否则需要验证码校验
		if(post_num!=1){
			String code=(String)session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
			
			//如果需要校验验证码时session却没有验证码存储，则表示用户又是在外部post，拦截
			if(code==null)
				throw new LoginException("Illegal invasion,you must stop!",session);
			
			//如果验证码校验(不区分大小写)不通过，提示用户验证码输入错误
			if(!code.equalsIgnoreCase(captcha))
				throw new LoginException("Wrong Captcha,please try again.",session);
		}
		
		//经过各种规则校验后，才允许进数据库检索
		System system=this.systemService.loginByUsernamePwd(userName, userPwd);
		if(system==null)
			throw new LoginException("Invalid userName and password!",session);
		else{
			session.setAttribute(BlogConfig.SYSTEM, system);
			return "redirect:article/articles";
		}
	}
	
	@RequestMapping(value="/quite",method=RequestMethod.GET)
	public String quite(HttpSession session){
		session.removeAttribute(BlogConfig.SYSTEM);
		session.removeAttribute(BlogConfig.POST_NUM);
		return "redirect:article/articles"; 
	}
}
