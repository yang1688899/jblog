<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.newflypig.jblog.model.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%=SystemStatic.BLOG_METAS %>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/common.css">
	<title><%=SystemStatic.BLOG_TITLE %></title>
	<style type="text/css">
			*{
				margin: 0px;
				padding: 0px;
			}
			body{
				background-color: #293134;
			}
			#banner{
				width: 100%;
				height: 300px;				
			}
			#banner h1,h2{
				color:#fff;								
				font: 36px "微软雅黑";
				letter-spacing: 2px;
			}
			#banner h1{
				display: inline-block;
				margin: 50px 0 0 100px;
			}
			#banner h2{
				display: block;
				margin: 50px 0 0 150px;
			}
			.articles h1{
				margin:15px 0 15px 5px;
			}
	</style>	
</head>
<%
	Pager<Article> pageArticles=(Pager<Article>)request.getAttribute("pageArticles");
%>
<body>
	<div id="banner">
		<h1>HexCode.cn | CodingCrafts.com</h1>
		<h2>一个全栈技术工匠的地下车间</h2>
	</div>
	<a href="<%=request.getContextPath() %>/article/add">添加文章</a>
	<a href="<%=request.getContextPath() %>/login">管理</a>
	<% if(session.getAttribute("system")!=null){ %>
		<a href="<%=request.getContextPath()%>/quite">退出</a>
	<% }
	%>
	<div id="pagers" style="margin-top:10px;"><span>共<%=pageArticles.getTotalPages()%>页：</span>
	<%
		for(String[] url:pageArticles.getUrls()){
	%>		
			<%if(Integer.valueOf(url[1])!=pageArticles.getCurrentPage()){%>
				<span><a href="<%=request.getContextPath()%>/article/articles?page=<%=url[1]%>"><%=url[0] %></a>
			<%}else{%>
				<span style="color:#5287b1"><%=url[0]%>
			<%}%>
			</span>
	<%
		}
	%>
	</div>
	<div class="articles">
	<%for(Article a : pageArticles.getData()) {%>
		<h1><%=a.getTitle() %>&nbsp;<span class="date"><%=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(a.getDate()) %></span>---<a href="<%=request.getContextPath() %>/article/<%=a.getArticleId()%>/delete">删除</a>
		---<a href="<%=request.getContextPath() %>/article/<%=a.getArticleId()%>/show">查看</a>
		---<a href="<%=request.getContextPath() %>/article/<%=a.getArticleId()%>/update">修改</a></h1>
	<%}%>
	</div>
	<div class="footer">
		<a href="http://www.miitbeian.gov.cn/" target="blank"><%=SystemStatic.BLOG_RECORD %></a>
		<%=SystemStatic.BLOG_SCRIPTS %>
	</div>
</body>
	<script type="text/javascript">
			function fixbanner () {				
				var width=(document.body.clientWidth-1920)/1.5;							
				window.banner.style.backgroundPosition="" +width+ "px 0";
			}

			window.onload=function () {
				window.banner=document.getElementById('banner');
				banner.style.backgroundImage='url("<%=request.getContextPath()%>/resources/img/banner.jpg")';
				fixbanner();
			};

			window.onresize=function() {
				fixbanner();
			};
	</script>
</html>